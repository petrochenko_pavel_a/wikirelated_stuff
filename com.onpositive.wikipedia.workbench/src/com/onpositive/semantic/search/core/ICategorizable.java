package com.onpositive.semantic.search.core;

public interface ICategorizable {

	public abstract ICategory[] getCategories();

	String getTitle();
}