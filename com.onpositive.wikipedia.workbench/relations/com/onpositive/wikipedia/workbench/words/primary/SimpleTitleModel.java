package com.onpositive.wikipedia.workbench.words.primary;

import java.util.List;

import com.onpositive.semantic.wordnet.AbstractWordNet;
import com.onpositive.semantic.wordnet.WordNetProvider;
import com.onpositive.text.analysis.IToken;
import com.onpositive.text.analysis.lexic.PrimitiveTokenizer;
import com.onpositive.text.analysis.lexic.WordFormParser;

public class SimpleTitleModel {

	
	SimpleTitleModel(String t){
		
	}
	
	public static List<IToken> getWordFormTokens(String str) {
		str=str.replace('_', ' ').toLowerCase();
		PrimitiveTokenizer pt = new PrimitiveTokenizer();
		AbstractWordNet instance = WordNetProvider.getInstance();
		WordFormParser wfParser = new WordFormParser(instance);
		wfParser.setIgnoreCombinations(true);
		List<IToken> tokens = pt.tokenize(str);		
		List<IToken> processed = wfParser.process(tokens);
		return processed;
	}
	
	public static String basicForm(IToken t){
//		if (t instanceof WordFormToken){
//			WordFormToken wft=(WordFormToken) t;
//			return wft.getBasicForm();
//		}
		return t.getShortStringValue();
	}
	
}
