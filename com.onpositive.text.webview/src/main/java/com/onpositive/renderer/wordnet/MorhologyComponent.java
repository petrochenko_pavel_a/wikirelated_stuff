package com.onpositive.renderer.wordnet;

import com.onpositive.text.webview.DemoComponent;

public class MorhologyComponent extends DemoComponent{

	@Override
	public String getDescription() {
		return "Снятие частоторечной омонимии. Введите строку для анализа:";
	}

	@Override
	public String getTitle() {
		return "Омонимия";
	}

	@Override
	public String getId() {
		return "omonimy";
	}

	@Override
	public String getOutput() {
		return "";
	}

}
